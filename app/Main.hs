{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules, DeriveGeneric #-}
module Main where

import Web.Spock
import Web.Spock.Config

import Control.Monad.Trans
import Data.Monoid
import Data.IORef
import qualified Data.Text as T

import qualified  Database.Bloodhound as BH
import GHC.Generics           (Generic)
import Network.HTTP.Client    (defaultManagerSettings,responseBody)

import Data.Time.Clock (UTCTime (..))
import Data.Aeson (eitherDecode,FromJSON (..), defaultOptions,genericParseJSON, genericToJSON,object,(.=),Value(String))

import Web.Spock.Lucid
import Lucid

data MySession = EmptySession
data MyAppState = DummyAppState (IORef Int)

esServer :: BH.Server
esServer = (BH.Server "http://localhost:9200")

runBH' :: BH.BH IO a -> IO a
runBH' = BH.withBH defaultManagerSettings esServer


data Location = Location { lat :: Double
                          , lon :: Double } deriving (Eq, Generic, Show)

data Item = Item { 
                     created :: UTCTime
                   , slug  :: T.Text
                   , title  :: T.Text
                   , body  :: T.Text
                   , price      :: Float
                   , location :: Location } deriving (Eq, Generic, Show)

instance BH.ToJSON Item where
 toJSON = genericToJSON defaultOptions

instance FromJSON Item where
 parseJSON = genericParseJSON defaultOptions

instance BH.ToJSON Location where
 toJSON = genericToJSON defaultOptions

instance FromJSON Location where
 parseJSON = genericParseJSON defaultOptions

main :: IO ()
main =
    do ref <- newIORef 0
       spockCfg <- defaultSpockCfg EmptySession PCNoDatabase (DummyAppState ref)
       runSpock 6000 (spock spockCfg app)

     
app :: SpockM () MySession MyAppState ()
app =
    do get root $ do
          text "Yep"
       get("d" <//> var) $ \itemId -> do
          re <- liftIO $ runBH' $ BH.getDocument (BH.IndexName "listed-items") (BH.MappingName "job") (BH.DocId itemId)
          let b = responseBody re
          let eitherResult = eitherDecode b :: Either String (BH.EsResult Item)
          case eitherResult of 
            Left err -> json $
              object
              [ "result" .= String "failure"
              , "error" .= object ["code" .= err]
              ]
            Right esR -> case (BH.foundResult esR) of
              Nothing -> text "Nothing"
              Just found -> lucidT $ (doctypehtml_ $ toHtml $ title $ BH._source found )
  
       get ("hello" <//> var) $ \name ->
           do (DummyAppState ref) <- getState
              visitorNumber <- liftIO $ atomicModifyIORef' ref $ \i -> (i+1, i+1)
              text ("Hello " <> name <> ", you are visitor number " <> T.pack (show visitorNumber))