{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric         #-}


module M 
( Post
,Reply
,SearchTerm
,Email
,Location
) where

import qualified  Database.Bloodhound as BH
import Data.Aeson (FromJSON (..), genericParseJSON, genericToJSON)
import qualified Data.Text as T
import Data.Time.Clock (UTCTime (..))
import GHC.Generics (Generic)

data Location = Location { lat :: Double
                         , lon :: Double 
                         } deriving (Eq, Generic, Show)


data Post = Person { created :: UTCTime
                   , email :: T.Text
                   , hash  :: String
                   , title  :: String
                   , body  :: T.Text
                   , location :: Location
                   , image :: T.Text
                   , attachment :: T.Text 
                   }
          | Job { created :: UTCTime
                , hash  :: String
                , title  :: String
                , body  :: T.Text
                , location :: Location
                } deriving (Eq, Generic, Show)


data Reply = Application{ created :: UTCTime
                        , email :: T.Text
                        , hash  :: T.Text
                        , post :: T.Text
                        , title  :: T.Text
                        , body  :: T.Text
                        , image :: T.Text
                        , attachment:: T.Text
                        } 
           | Offer{ created :: UTCTime
                  , email :: T.Text
                  , hash  :: T.Text
                  , post :: T.Text
                  , title  :: T.Text
                  , body  :: T.Text
                  } deriving (Eq, Generic, Show)

instance BH.ToJSON Post where
 toJSON = genericToJSON defaultOptions

instance FromJSON Post where
 parseJSON = genericParseJSON defaultOptions

instance BH.ToJSON Reply where
 toJSON = genericToJSON defaultOptions

instance FromJSON Reply where
 parseJSON = genericParseJSON defaultOptions

instance BH.ToJSON Location where
 toJSON = genericToJSON defaultOptions

instance FromJSON Location where
 parseJSON = genericParseJSON defaultOptions