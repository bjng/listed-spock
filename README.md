# listed-spock

This is a personal study project for me to learn:

* Haskell
* Creating Web Apps in Haskell
* Elasticsearch
* Docker for Cloud distribution of Web App
* Deploying the Haskell-Elasticsearch App on Redhats Openshift 3

In the moment this repo only contains the Haskell part.

This is my learning path

Elasticsearch:
* ~~Install and run Elastisearch~~
* ~~Learn Concepts Basic CRUD methods~~
* ~~Create a simple test data for Webapp~~

Haskell and Spock:

* ~~Learn basic Haskel (with "Learn You a Hakell for Great Good")~~
* ~~Reasearch WebApp Frameworks for Haskell (picked Spock)~~
* ~~Setup and run Spock~~
* ~~Render JSON with Spock~~
* ~~Render HTML with Spock (with Lucid)~~
* ~~Get Elasticsearch Data via Bloodhound Lib)~~
* ~~Serve Data from ES as Json or HTML~~ 
* Setup  simple folder and module structure to seperate views and routes
* Send Emails via Spock
* Setup Authorisation Flow

Docker:

* ~~Setup and run Docker~~
* Getting started Tutorial
  - ~~Part I~~
  - ~~Part II~~
  - ~~Part III~~
  - ~~Part IV~~
  - ~~Part V~~
  - Part VI
* ~~Create Elasticsearch Spock Composition and Services~~

Openshift:

* ~~Study basic concepts~~
* ~~Running and changing basic Webapp from Tutorial (Node.js, Mongo)~~
* Find out how to run and compose Docker Images for Spock and Elasticsearch
* Create necessary Configurations
* Deploy 

