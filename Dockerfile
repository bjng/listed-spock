# Use an official Python runtime as a base image
FROM haskell:latest

# Set the working directory to /app
WORKDIR /spock

# Copy the current directory contents into the container at /app
ADD . /spock

# Install any needed packages specified in requirements.txt
RUN stack update &&\ 
	stack build --fast --pedantic

# Make this port available to the world outside this container
EXPOSE 8080

# Define environment variable
# ENV NAME World

# Run app.py when the container launches
CMD ["stack", "exec","workinseason-exe"]